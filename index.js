let currentDate = new Date();
let fullDateString = currentDate.toISOString().split("T")[0];

let index;
let arr = [];

window.addEventListener("load", function () {
  let storedArr = localStorage.getItem("tasks");
  storedArr = JSON.parse(storedArr)[fullDateString];
  const tempArr = [];
  storedArr.forEach((element, index) => {
    tempArr.push(element);
    makeTemplate({ value: element }, index, tempArr);
  });
});

const submit = document.querySelector("#enter");
submit.addEventListener("click", function () {
  let input = document.querySelector("#item");
  if (input.value.trim() == "") {
    return;
  }
  arr.push(input.value.trim());
  index = arr.length - 1;
  makeTemplate(input, index, arr);

  localStorage.setItem(
    "tasks",
    JSON.stringify({
      [fullDateString]: arr,
    })
  );
});

function makeTemplate(input, index, arr) {
  let tasks = document.querySelector(".tasks");
  if (tasks.innerHTML == "") {
    const instruction = document.querySelector(".instruction");
    instruction.style.display = "none";
  }
  const items = document.createElement("div");
  const inputValue = input.value.trim();
  let id;
  if (inputValue.length < 7) {
    id = inputValue + arr.length;
  } else {
    id = inputValue.substring(4, 7) + inputValue.substring(2, 6) + arr.length;
  }
  items.innerHTML = `
            <input type="checkbox" id="myCheckbox-${id}">
             <div class="task uncheck">${input.value.trim()}</div>
            <button class="delete-btn">Delete</button>
            <button class="edit-btn">Edit</button>
            <button class="save-btn">Save</button>
        `;
  tasks.appendChild(items);
  const task = items.querySelector(".task");

  items.querySelector(".delete-btn").addEventListener("click", function () {
    tasks.removeChild(items);
    let ind;
    arr.forEach((element, index) => {
      if (element == task.textContent) {
        ind = index;
      }
    });
    arr.splice(ind, 1);
    localStorage.setItem(
      "tasks",
      JSON.stringify({
        [fullDateString]: arr,
      })
    );
  });

  items.querySelector(".edit-btn").addEventListener("click", function () {
    task.contentEditable = true;
    task.style.border = "1px solid black";
    console.log(task.style.borderColor, "null");
  });

  items.querySelector(".save-btn").addEventListener("click", function () {
    task.style.border = "none";
    const str = task.textContent;
    console.log(index);
    arr[index] = task.textContent;
    localStorage.setItem(
      "tasks",
      JSON.stringify({
        [fullDateString]: arr,
      })
    );
    task.contentEditable = false;
  });
  let checkbox = document.getElementById(`myCheckbox-${id}`);
  checkbox.addEventListener("change", function () {
    console.log("check", checkbox);
    if (checkbox.checked) {
      task.classList.remove("uncheck");
      task.classList.add("check");
    } else {
      task.classList.remove("check");
      task.classList.add("uncheck");
    }
  });
  input.value = "";
}

const done = document.querySelector(".completed");
done.addEventListener("click", function () {
  let tasks = document.querySelector(".tasks");
  console.log(tasks.childNodes);
  for (let node of tasks.childNodes) {
    console.log(
      node.childNodes[3],
      Array.isArray(node.childNodes[3].classList)
    );
    if (node.childNodes[3].classList.contains("uncheck")) {
      node.style.display = "none";
    } else {
      node.style.display = "flex";
    }
  }
});

function todo() {
  let tasks = document.querySelector(".tasks");
  console.log(tasks.childNodes);
  for (let node of tasks.childNodes) {
    if (node.childNodes[3].classList.contains("check")) {
      node.style.display = "none";
    } else {
      node.style.display = "flex";
    }
  }
}

const all = document.querySelector(".all-task");
all.addEventListener("click", function () {
  console.log(`calling`);
  let tasks = document.querySelector(".tasks");
  for (let node of tasks.childNodes) {
    node.style.display = "flex";
  }
});

const clearAll = document.querySelector(".clearAll");
clearAll.addEventListener("click", function () {
  let tasks = document.querySelector(".tasks");
  tasks.innerHTML = "";
  const instruction = document.querySelector(".instruction");
  instruction.style.display = "block";
  localStorage.setItem(
    "tasks",
    JSON.stringify({
      [fullDateString]: [],
    })
  );
});

